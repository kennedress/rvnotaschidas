package com.fcfm.fragmentos.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.fcfm.fragmentos.CreateFragment
import com.fcfm.fragmentos.FavoritesFragment
import com.fcfm.fragmentos.NotesFragment
import com.fcfm.fragmentos.R

class FragmentAdapter(private val mContext: Context, fm: FragmentManager)
    : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    val createFragment : CreateFragment = CreateFragment.newInstance()
    val notesFragment : NotesFragment = NotesFragment.newInstance()
    val favoritesFragment : FavoritesFragment = FavoritesFragment.newInstance()
    private val countTab = 3

    override fun getItem(position: Int): Fragment {
        when(position){
            0 -> {return createFragment}
            1 -> { return notesFragment }
            2 -> {return favoritesFragment}
            else -> {return createFragment}
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position){
            0 -> { return mContext.resources.getString(R.string.create_title) }
            1 -> { return mContext.resources.getString(R.string.notes_title) }
            2 -> { return mContext.resources.getString(R.string.favorites_title) }
            else -> { return mContext.resources.getString(R.string.create_title) }
        }
    }

    override fun getCount(): Int {
        return countTab
    }
}