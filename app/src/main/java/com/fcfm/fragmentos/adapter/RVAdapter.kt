package com.fcfm.fragmentos.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fragmentos.MainActivity
import com.fcfm.fragmentos.R
import com.fcfm.fragmentos.model.Note
import kotlinx.android.synthetic.main.viewholder_item.view.*

class RVAdapter(private val context: Context, private val notesList: ArrayList<Note>, private val isFavorite: Boolean) :
        RecyclerView.Adapter<RVAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.viewholder_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTitle.text = notesList[position].title
        holder.tvDescription.text = notesList[position].description
        holder.btnFavoritos.setOnClickListener {
            (context as MainActivity).noteList[position].favorito = true
            context.adapter.favoritesFragment.updateList()
            notifyDataSetChanged()
        }
        if(isFavorite){
            holder.btnFavoritos.visibility = View.GONE
            if (!notesList[position].favorito) {
                val param = holder.clItem.layoutParams
                param.height = 0
                holder.clItem.layoutParams = param
                holder.clItem.visibility = View.GONE
            }
        }else{
            //holder.btnFavoritos.visibility = View.VISIBLE
            if (notesList[position].favorito) {
                val param = holder.clItem.layoutParams
                param.height = 0
                holder.clItem.layoutParams = param
                holder.clItem.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return notesList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val tvTitle = itemView.tv_title
        val tvDescription = itemView.tv_description
        val btnFavoritos = itemView.btn_add_favorites
        val clItem = itemView.cl_item
    }

}