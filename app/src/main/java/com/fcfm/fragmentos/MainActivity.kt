package com.fcfm.fragmentos

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fcfm.fragmentos.adapter.FragmentAdapter
import com.fcfm.fragmentos.model.Note
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_create.*

class MainActivity : AppCompatActivity(),
        CreateFragment.OnFragmentInteractionListener,
        FavoritesFragment.OnFragmentInteractionListener,
        NotesFragment.OnFragmentInteractionListener{

    lateinit var adapter: FragmentAdapter
    var noteList = arrayListOf<Note>()

    override fun onFragmentInteraction(uri: Uri) { }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadTab()
    }

    private fun loadTab(){
        adapter = FragmentAdapter(this, supportFragmentManager)
        vp_pager!!.adapter = adapter
        tl_tab!!.setupWithViewPager(vp_pager)

        tl_tab.getTabAt(0)
        tl_tab.getTabAt(1)
        tl_tab.getTabAt(2)

    }


}
