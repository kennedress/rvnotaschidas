package com.fcfm.fragmentos

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.fcfm.fragmentos.model.Note
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.fragment_create.*

class CreateFragment : Fragment() {
    private lateinit var mContext: Context
    private lateinit var titulo: TextInputEditText
    private lateinit var contenido: TextInputEditText
    private lateinit var guardar: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val inflate = inflater.inflate(R.layout.fragment_create, container, false)
        initController(inflate)
        return inflate
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FavoritesFragment.OnFragmentInteractionListener) {
            mContext = context
        } else {
            throw RuntimeException(context.toString())
        }
    }

    interface OnFragmentInteractionListener { fun onFragmentInteraction(uri: Uri) }

    private fun initController(inflate: View){
        guardar = inflate.findViewById(R.id.btn_save)
        titulo = inflate.findViewById(R.id.tiet_title)
        contenido = inflate.findViewById(R.id.tiet_description)

        guardar.setOnClickListener {
            val title = titulo.text.toString()
            val desc = contenido.text.toString()

            (mContext as MainActivity).noteList.add(Note(title, desc, false))
            (mContext as MainActivity).adapter.notesFragment.updateList()
            Toast.makeText(mContext, "Nota agregada con éxito", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() : CreateFragment {
            return CreateFragment()
        }
    }
}
