package com.fcfm.fragmentos

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fragmentos.adapter.RVAdapter
import com.fcfm.fragmentos.model.Note

class NotesFragment : Fragment() {

    private lateinit var mContext: Context
    private lateinit var adapter: RVAdapter
    private lateinit var rvNotes: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val inflate = inflater.inflate(R.layout.fragment_notes, container, false)
        initController(inflate)
        return inflate
    }

    private fun initController(inflate: View){
        rvNotes = inflate.findViewById(R.id.rv_notes)
        val llm = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        rvNotes.layoutManager = llm
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FavoritesFragment.OnFragmentInteractionListener) {
            mContext = context
        } else {
            throw RuntimeException(context.toString())
        }
    }

    fun updateList(){
        if ((mContext as MainActivity).noteList.size == 1) {
            val contextt = mContext
            val lista = (mContext as MainActivity).noteList
            adapter = RVAdapter(contextt, lista, false)
            rvNotes.adapter = adapter
        }else{
            rvNotes.adapter!!.notifyDataSetChanged()
        }
    }

    interface OnFragmentInteractionListener { fun onFragmentInteraction(uri: Uri) }

    companion object {

        fun newInstance() : NotesFragment {
            return NotesFragment()
        }
    }
}
