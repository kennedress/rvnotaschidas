package com.fcfm.fragmentos

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fragmentos.adapter.RVAdapter

class FavoritesFragment : Fragment() {

    private lateinit var mContext: Context
    private lateinit var adapter: RVAdapter
    private lateinit var rvFavorites: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val inflate = inflater.inflate(R.layout.fragment_favorites, container, false)
        initController(inflate)
        return inflate
    }

    private fun initController(inflate: View){
        rvFavorites = inflate.findViewById(R.id.rv_favorites)
        val llm = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        rvFavorites.layoutManager = llm
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mContext = context
        } else {
            throw RuntimeException(context.toString())
        }
    }

    fun updateList(){
        val contextt = mContext
        val lista = (mContext as MainActivity).noteList
        adapter = RVAdapter(contextt, lista, true)
        rvFavorites.adapter = adapter

    }

    interface OnFragmentInteractionListener { fun onFragmentInteraction(uri: Uri) }

    companion object {
        @JvmStatic
        fun newInstance() : FavoritesFragment {
            return FavoritesFragment()
        }
    }
}
